var express = require('express');
var router = express.Router();
var multer  = require('multer');

var Client = require('../models/Client');
var Gallery = require('../models/Gallery.js');

const {PDFDocument, StandardFonts, rgb, cmyk, degrees} = require('pdf-lib');
const fs = require('fs');
const fetch = require("node-fetch");
const path = require('path');
const url = require('url');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/output');
    },
    filename: (req, file, cb) => {
        console.log(file);
        let filetype = '';
        if(file.mimetype === 'application/pdf') {
            filetype = 'pdf';
        }
        cb(null, 'pdfTest');
    }
});

var upload = multer({storage: storage});

router.post('/generate', upload.none(), function (req, res, next) {
    var path = './public/output/';
    const stats = fs.statSync(path + 'test.pdf');
    let fileSize = stats.size / (1024*1024);

    fileSize = (Math.round(fileSize * 100) / 100).toFixed(4);

    req.body.imageUrl = "pdf-" + Date.now() + ".pdf";
    req.body.imageDesc = fileSize + " MB";

    fs.rename(path + 'test.pdf', `./public/images/${req.body.imageUrl}`, (err) => {
        if (err) {
            console.log(err);
        }
    });

    console.log(req.body);

    Gallery.create(req.body, function (err, gallery) {
        if (err) {
            console.log(err);
        }
        else {
            console.log('PDF imported successfully');
        }
        res.json(gallery);
    });
});

router.post('/', upload.none(), async (req, res) => {

    let uint8Array;
    switch (req.body.pick) {
        case '0':
            uint8Array = fs.readFileSync('./public/templates/Template1.pdf');
            break;
        case '1':
            uint8Array = fs.readFileSync('./public/templates/Template2.pdf');
            break;
        case '2':
            uint8Array = fs.readFileSync('./public/templates/Template3.pdf');
            break;
        default:
    }

    const pdfDoc = await PDFDocument.load(uint8Array);
    const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica);

    const pages = pdfDoc.getPages();
    const firstPage = pages[0];
    const { width, height } = firstPage.getSize();

    const nowDate = new Date(Date.now());
    const date = nowDate.getDate() + '/' + (nowDate.getMonth()+1)  + '/' + nowDate.getFullYear();
    nowDate.setDate(nowDate.getDate() + 6);
    const date2 = nowDate.getDate() + '/' + (nowDate.getMonth()+1)  + '/' + nowDate.getFullYear();
    const invoiceNumber = "INT-" + Date.now();

    const amount = Number(req.body.value) * Number(req.body.quantity);
    const tvaValue = 19/100 * amount;
    const total = amount + tvaValue;

    firstPage.setFontSize(9);
    firstPage.setFont(helveticaFont);
    firstPage.setFontColor(rgb(0, 0, 0));

    switch (req.body.pick) {
        case '0':
        // Client's INFO - first set
            firstPage.moveTo(35, height - 225);
            firstPage.drawText(req.body.company);

            firstPage.moveTo(35, height - 238);
            firstPage.drawText(req.body.owner);

            firstPage.moveTo(35, height - 251);
            firstPage.drawText(req.body.adress);

            firstPage.moveTo(35, height - 264);
            firstPage.drawText(req.body.cif);

        // Client's INFO - second set
            firstPage.moveRight(158);
            firstPage.drawText(req.body.cif);

            firstPage.moveUp(13);
            firstPage.drawText(req.body.adress);

            firstPage.moveUp(13);
            firstPage.drawText(req.body.owner);

            firstPage.moveUp(13);
            firstPage.drawText(req.body.company);

        // Invoice's Date
            firstPage.setFontSize(14);
            firstPage.setFontColor(cmyk(1, 0.5, 0, 0.6));

            firstPage.moveRight(245);
            firstPage.drawText(date.toString());

            firstPage.moveUp(20);
            firstPage.moveLeft(20);
            firstPage.drawText(invoiceNumber.toString());

            firstPage.moveDown(42);
            firstPage.drawText(date2.toString());

        //Item/service, qunatity, unit, total...
            firstPage.resetPosition();
            firstPage.setFontColor(rgb(0, 0 ,0));

            firstPage.moveTo(65, height - 348);
            firstPage.drawText(req.body.quantity);

            firstPage.moveRight(100);
            firstPage.drawText(req.body.item);

            firstPage.moveRight(205);
            firstPage.drawText(req.body.value + req.body.unit);

            firstPage.moveRight(137);
            firstPage.drawText(amount.toString() + req.body.unit);

            firstPage.moveDown(30);
            firstPage.drawText(amount.toString() + req.body.unit);

            firstPage.moveDown(27);
            firstPage.drawText(tvaValue.toString() + req.body.unit);

            firstPage.setFontColor(cmyk(1, 0.5, 0, 0.6));
            firstPage.moveDown(27);
            firstPage.drawText(total.toString() + req.body.unit);
            break;
        case '1':
            // Client's INFO - first set
            firstPage.moveTo(39, height - 245);
            firstPage.drawText(req.body.company);

            firstPage.moveDown(13);
            firstPage.drawText(req.body.owner);

            firstPage.moveDown(13);
            firstPage.drawText(req.body.adress);

            firstPage.moveDown(13);
            firstPage.drawText(req.body.cif);

            // Client's INFO - second set
            firstPage.moveRight(171);
            firstPage.drawText(req.body.cif);

            firstPage.moveUp(13);
            firstPage.drawText(req.body.adress);

            firstPage.moveUp(13);
            firstPage.drawText(req.body.owner);

            firstPage.moveUp(13);
            firstPage.drawText(req.body.company);

            // Invoice's Date
            firstPage.setFontSize(14);
            firstPage.setFontColor(cmyk(1, 0.5, 0, 0.6));

            firstPage.moveRight(245);
            firstPage.moveDown(5);
            firstPage.drawText(date.toString());

            firstPage.moveUp(20);
            firstPage.moveLeft(20);
            firstPage.drawText(invoiceNumber.toString());

            firstPage.moveDown(42);
            firstPage.drawText(date2.toString());

            //Item/service, qunatity, unit, total...
            firstPage.resetPosition();
            firstPage.setFontColor(rgb(0, 0 ,0));

            firstPage.moveTo(50, height - 437);
            firstPage.drawText(req.body.quantity);

            firstPage.moveRight(100);
            firstPage.drawText(req.body.item);

            firstPage.moveRight(205);
            firstPage.drawText(req.body.value + req.body.unit);

            firstPage.moveRight(137);
            firstPage.drawText(amount.toString() + req.body.unit);

            firstPage.moveDown(30);
            firstPage.drawText(amount.toString() + req.body.unit);

            firstPage.moveDown(27);
            firstPage.drawText(tvaValue.toString() + req.body.unit);

            firstPage.setFontColor(cmyk(1, 0.5, 0, 0.6));
            firstPage.moveDown(27);
            firstPage.drawText(total.toString() + req.body.unit);
            break;
        case '2':
            // Client's INFO - first set
            firstPage.moveTo(39, height - 245);
            firstPage.drawText(req.body.company);

            firstPage.moveDown(13);
            firstPage.drawText(req.body.owner);

            firstPage.moveDown(13);
            firstPage.drawText(req.body.adress);

            firstPage.moveDown(13);
            firstPage.drawText(req.body.cif);

            // Client's INFO - second set
            firstPage.moveRight(171);
            firstPage.drawText(req.body.cif);

            firstPage.moveUp(13);
            firstPage.drawText(req.body.adress);

            firstPage.moveUp(13);
            firstPage.drawText(req.body.owner);

            firstPage.moveUp(13);
            firstPage.drawText(req.body.company);

            // Invoice's Date
            firstPage.setFontSize(14);
            firstPage.setFontColor(cmyk(1, 0.5, 0, 0.6));

            firstPage.moveRight(245);
            firstPage.moveDown(8);
            firstPage.drawText(date.toString());

            firstPage.moveUp(20);
            firstPage.moveLeft(20);
            firstPage.drawText(invoiceNumber.toString());

            firstPage.moveDown(42);
            firstPage.drawText(date2.toString());

            //Item/service, qunatity, unit, total...
            firstPage.resetPosition();
            firstPage.setFontColor(rgb(0, 0 ,0));

            firstPage.moveTo(60, height - 370);
            firstPage.drawText(req.body.quantity);

            firstPage.moveRight(120);
            firstPage.drawText(req.body.item);

            firstPage.moveRight(185);
            firstPage.drawText(req.body.value + req.body.unit);

            firstPage.moveRight(137);
            firstPage.drawText(amount.toString() + req.body.unit);

            firstPage.moveDown(30);
            firstPage.drawText(amount.toString() + req.body.unit);

            firstPage.moveDown(27);
            firstPage.drawText(tvaValue.toString() + req.body.unit);

            firstPage.setFontColor(cmyk(1, 0.5, 0, 0.6));
            firstPage.moveDown(27);
            firstPage.drawText(total.toString() + req.body.unit);
            break;
        default:
    }

    const pdfBytes = await pdfDoc.save(err => {
        if (!err) {
            console.log('Invoice-ul a fost generat.');
        } else {
            console.log('Eroare in generare invoice-ului: ' + JSON.stringify(err, undefined, 2));
        }
    });

    fs.writeFileSync('./public/output/test.pdf', pdfBytes);
    res.contentType("application/pdf");
    res.send(pdfBytes);
});

module.exports = router;
