var express = require('express');
var router = express.Router();
var Client = require('../models/Client');

var ObjectId = require('mongoose').Types.ObjectId;

router.get('/', (req, res) => {
    Client.find((err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error in retriving documents: ' + JSON.stringify(err, undefined, 2));
        }
    });
});

router.get('/:id', (req, res) => {
    Client.findById(req.params.id, (err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log('Error in retriving client: ' + JSON.stringify(err, undefined, 2));
        }
    });
});

router.delete('/:id', (req, res) => {
   if (!ObjectId.isValid(req.params.id)) {
       return res.status(400).send(`No record with this id: ${req.params.id}`)
   }
   Client.findByIdAndRemove(req.params.id, (err, doc) => {
       if (err) {
           console.log('Error in deleting document: ' + JSON.stringify(err, undefined, 2));
       } else {
           res.send(doc);
       }
   })
});

module.exports = router;
