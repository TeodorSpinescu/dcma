var express = require('express');
var router = express.Router();
var multer  = require('multer');
var Gallery = require('../models/Gallery.js');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/images');
    },
    filename: (req, file, cb) => {
        console.log(file);
        let filetype = '';
        if(file.mimetype === 'image/gif') {
            filetype = 'gif';
        }
        if(file.mimetype === 'image/png') {
            filetype = 'png';
        }
        if(file.mimetype === 'image/jpeg') {
            filetype = 'jpg';
        }
        if(file.mimetype === 'application/pdf') {
            filetype = 'pdf';
        }
        // cb(null, 'image-' + Date.now() + '.' + filetype);
        cb(null, file.originalname);
    }
});

var upload = multer({storage: storage});

router.post('/', upload.single('file'), function(req, res, next) {
    if(!req.file) {
        return res.status(500).send({ message: 'Upload fail'});
    } else {
        req.body.imageUrl = req.file.filename;

        let fileSize = req.file.size / (1024*1024);
        fileSize = (Math.round(fileSize * 100) / 100).toFixed(2);

        req.body.imageDesc = fileSize + " MB";
        Gallery.create(req.body, function (err, gallery) {
            if (err) {
                console.log(err);
                return next(err);
            }
            res.json(gallery);
        });
    }
});

module.exports = router;
