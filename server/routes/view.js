var express = require('express');
var router = express.Router();
var Gallery = require('../models/Gallery.js');
var Client = require('../models/Client');

const fs = require('fs');
const path = require('path');
const { createWorker } = require('tesseract.js');
const PSM = require('tesseract.js/src/constants/PSM.js');

var ObjectId = require('mongoose').Types.ObjectId;

const rectangles = [
    {
        left: 46,
        top: 115,
        width: 300,
        height: 240,
    },
    {
        left: 921,
        top: 115,
        width: 450,
        height: 170,
    },
    {
        left: 1396,
        top: 1125,
        width: 230,
        height: 90,
    },
    {
        left: 1175,
        top: 390,
        width: 200,
        height: 100,
    },
    {
        left: 1261,
        top: 632,
        width: 200,
        height: 80,
    },
];


router.get('/', (req, res) => {
    Gallery.find((err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error in retriving documents: ' + JSON.stringify(err, undefined, 2));
        }
    });
});

router.post('/extract/:id', (req, res) => {
    Gallery.findById(req.params.id, (err, doc) => {
        if (!err) {
            const imageUrl = doc.imageUrl;
            const worker = createWorker();

            (async () => {
                await worker.load();
                await worker.loadLanguage('eng');
                await worker.initialize('eng');

                const values = [];
                for (let i = 0; i < rectangles.length; i++) {
                    const { data: { text } } = await worker.recognize(`./public/images/${imageUrl}`, { rectangle: rectangles[i] });
                    values.push(text);
                }
// Characters Manipulation
                var companyDetails = values[0];
                var companyDetailsSplit = companyDetails.split("\n");


                var invoiceValue = values[2].slice(0, values[2].length - 1);
                var cif = values[3].slice(0, values[3].length - 1);
                var date = values[4].slice(0, values[4].length - 1);

                var client = new Client ({
                    companyName: companyDetailsSplit[0],
                    ownerName: companyDetailsSplit[1],
                    adress: companyDetailsSplit[2] + ' ' + companyDetailsSplit[3],
                    cifNumber: cif,
                    total: invoiceValue,
                    invoiceDate: date
                });

                client.save(err => {
                    if (!err) {
                        console.log('Clientul a fost salvat cu succes.');
                    } else {
                        console.log('Eroare in salvarea Clientului: ' + JSON.stringify(err, undefined, 2));
                    }
                });
                console.log(values);
                await worker.terminate();
            })();
        } else {
            console.log('Error in extracting data: ' + JSON.stringify(err, undefined, 2));
        }
    });
});

router.get('/download/:id', (req, res) => {
    Gallery.findById(req.params.id, (err, doc) => {
        if (!err) {
            const imageUrl = doc.imageUrl;
            const imgPath = path.resolve(`${__dirname}`, `./../public/images/${imageUrl}`);
            res.download(imgPath);

            /*const imgData = fs.readFileSync(imgPath);
            // [`Content-Disposition: attachment; filename=${imageUrl}`]
            res
                .status(200)
                .type('image/jpeg')
                .send(imgData);*/
        } else {
            console.log('Error in retriving documents: ' + JSON.stringify(err, undefined, 2));
        }
    })
});

router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id)) {
        return res.status(400).send(`No record with this id: ${req.params.id}`)
    }
    Gallery.findByIdAndDelete(req.params.id, (err, doc) => {
        if (err) {
            console.log('Error in deleting document: ' + JSON.stringify(err, undefined, 2));
        } else {
            res.send(doc);
        }
    })
});


module.exports = router;
