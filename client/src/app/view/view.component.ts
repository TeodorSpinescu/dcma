import {Component, OnInit} from '@angular/core';
import {UploadService, UserService} from '../_services';

import {Gallery, User} from '../_models';
import {DownloadService} from '../_services/download.service';
import {ExtractService} from '../_services/extract.service';

import {faDownload, faFileExport, faTrashAlt} from '@fortawesome/free-solid-svg-icons';

import {saveAs} from 'file-saver';

import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
  providers: [UploadService]
})
export class ViewComponent implements OnInit {

  downloadIcon = faDownload;
  extractIcon = faFileExport;
  deleteIcon = faTrashAlt;

  currentUser: User;

  constructor(
    public uploadService: UploadService,
    private userService: UserService,
    private downloadService: DownloadService,
    private extractService: ExtractService,

    private toastr: ToastrService
  ) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit(): void {
    this.refreshDocumentsList();
  }

  refreshDocumentsList(): void {
    this.uploadService.getDocumentsList().subscribe((res) => {
      this.uploadService.documents = res as Gallery[];
    });
  }

  download(id: string): void {
    this.downloadService.downloadFile(id).subscribe(
      response => {
        console.log(response);
        const type = response.headers.get('content-type');
        const ext = type.substring(type.lastIndexOf('/') + 1);

        const blob = new Blob([response.body]);
        saveAs(blob, `download.${ext}`);
      },
        error => console.log(error),
      () => console.log('File downloaded successfully'));
    this.toastr.success('File Downloaded', 'Succes', {
      timeOut: 6000,
      easeTime: 9000,
      positionClass: 'toast-bottom-right'
    });
  }

  extract(id: string): void {
    this.extractService.extractData(id).subscribe();
    this.toastr.info('A client has been extracted from the invoice.', 'New Client', {
      timeOut: 6000,
      positionClass: 'toast-bottom-right'
    });
  }

  isPNG(itemTitle: string): boolean {
    const split = itemTitle.split('.');
    return split[1] === 'png';
  }

  deleteFile(id: string): void {
    this.downloadService.deleteFile(id).subscribe( res => {
      location.reload();
    });
    this.toastr.warning('Your File has been deleted', 'Delete', {
      timeOut: 6000,
      positionClass: 'toast-bottom-right'
    });
  }
}
