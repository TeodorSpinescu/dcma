// import { Component, OnInit } from '@angular/core';
//
// import { UploadService} from '../_services';
// import { Router } from '@angular/router';
// import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
// import { ErrorStateMatcher } from '@angular/material/core';
// import {HttpEventType} from '@angular/common/http';
// import {Gallery} from '../_models';
//
// /** Error when invalid control is dirty, touched, or submitted. */
// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const isSubmitted = form && form.submitted;
//     return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
//   }
// }
//
// @Component({
//   selector: 'app-gallery',
//   templateUrl: './gallery.component.html',
//   styleUrls: ['./gallery.component.css']
// })
//
// export class GalleryComponent implements OnInit {
//
//   galleryForm: FormGroup;
//   imageFile: File = null;
//   imageTitle = '';
//   imageDesc = '';
//   isLoadingResults = false;
//   matcher = new MyErrorStateMatcher();
//
//   gallery = Gallery;
//
//   constructor(
//     private api: UploadService,
//     private formBuilder: FormBuilder,
//     private router: Router
//   ) { }
//
//   ngOnInit(): void {
//     this.galleryForm = this.formBuilder.group({
//       imageFile : [null, Validators.required],
//       imageTitle : [null, Validators.required],
//       imageDesc : [null, Validators.required]
//     });
//   }
//
//   onFormSubmit(): void {
//     this.isLoadingResults = true;
//     this.api.addGallery(this.galleryForm.value, this.galleryForm.get('imageFile').value._files[0])
//       .subscribe((res) => {
//         this.isLoadingResults = false;
//
//
//         if (res) {
//           this.router.navigate(['/gallery-details']);
//         }
//         // const cevaString = response.body;
//         // const resSTR = JSON.stringify(response.body);
//         // resSTR.replace('undifined', '');
//         // const resJSON = JSON.parse(resSTR);
//         // console.log(resJSON.imageTitle);
//         // console.log(resSTR);
//         // const body = response.body;
//         // console.log(body);
//         // console.log(response.type);
//         // if (response.type === HttpEventType.Response) {
//         //   console.log(response);
//         // }
//
//
//         // if (res.body) {
//         //   this.router.navigate(['/gallery-details', res.body]);
//         // }
//       }, (err: any) => {
//         console.log('Ceva nu e bine boys', err);
//         this.isLoadingResults = false;
//       });
//   }
//
// }
