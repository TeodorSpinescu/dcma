export class Gallery {
  _id: string;
  imageUrl: string;
  fileUser: string;
  imageDesc: string;
  uploaded: Date;
}
