export class Client {
  _id: string;
  companyName: string;
  ownerName: string;
  adress: string;
  cifNumber: string;
  total: string;
  invoiceDate: string;
}
