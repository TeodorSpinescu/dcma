export class PdfExtra {
  _id: string;
  item: string;
  quantity: string;
  value: string;
  unit: string;
  company: string;
  owner: string;
  adress: string;
  cif: string;
  pick: string;
}
