﻿import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {
  faExchangeAlt,
  faFileExport,
  faFileInvoice,
  faFileUpload, faFlag, faHome,
  faList, faPaperPlane, faSearch, faStream, faUserAlt,
} from '@fortawesome/free-solid-svg-icons';
import {InvoiceComponent} from '../invoice/invoice.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  opened = false;
  mainPage = faHome;
  fileUpload = faFileUpload;
  fileExport = faFileExport;
  viewFiles = faList;
  pdfCreator = faExchangeAlt;
  invoiceGenerator = faFileInvoice;
  userProfile = faUserAlt;
  sidenavToggle = faStream;
  searchTool = faSearch;
  mailNotification = faPaperPlane;
  flag = faFlag;


  constructor() { }

  ngOnInit(): void {
  }
}





// import { Component, OnInit } from '@angular/core';
//
// import { User } from '../_models';
// import { UserService } from '../_services';
//
// @Component({
//     selector: 'app-home',
//     templateUrl: 'home.component.html'
// })
//
// export class HomeComponent implements OnInit {
//     currentUser: User;
//     users: User[] = [];
//
//     constructor(private userService: UserService) {
//         this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
//     }
//
//   // tslint:disable-next-line:typedef
//     ngOnInit() {
//         this.loadAllUsers();
//     }
//
//
//   // tslint:disable-next-line:variable-name typedef
//     deleteUser(_id: string){
//         this.userService.delete(_id).subscribe(() => { this.loadAllUsers(); });
//     }
//
//
//   // tslint:disable-next-line:typedef
//     private loadAllUsers() {
//         this.userService.getAll().subscribe(users => { this.users = users; });
//     }
// }
