import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ImportComponent} from '../import/import.component';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MaterialModuleModule} from '../material-module/material-module.module';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatBadgeModule} from '@angular/material/badge';

import {MatMenuModule} from '@angular/material/menu';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HomeComponent} from './home.component';
import {SidenavComponent} from '../sidenav/sidenav.component';
import {MainComponent} from '../main/main.component';
import {MatInputModule} from '@angular/material/input';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {ViewComponent} from '../view/view.component';
import {CreatorComponent} from '../creator/creator.component';
import {InvoiceComponent} from '../invoice/invoice.component';
import {ProfileComponent} from '../profile/profile.component';
import {ExportComponent} from '../export/export.component';
import {ExtractService} from '../_services/extract.service';



@NgModule({
  declarations: [
    HomeComponent,
    SidenavComponent,
    MainComponent,
    ImportComponent,
    ExportComponent,
    ViewComponent,
    CreatorComponent,
    InvoiceComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NoopAnimationsModule,
    MaterialModuleModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatBadgeModule,
    MatMenuModule,
    FormsModule,
    FontAwesomeModule,

    MatInputModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MaterialFileInputModule,

    RouterModule,
  ],
  exports: [
    SidenavComponent
  ],
  providers: []
})
export class HomeModule { }
