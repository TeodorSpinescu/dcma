﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';


import {SidenavComponent} from './sidenav/sidenav.component';
import {MainComponent} from './main/main.component';
import {ImportComponent} from './import/import.component';
import {ViewComponent} from './view/view.component';
import {CreatorComponent} from './creator/creator.component';
import {InvoiceComponent} from './invoice/invoice.component';
import {ProfileComponent} from './profile/profile.component';
import {ExportComponent} from './export/export.component';

const appRoutes: Routes = [
    { path: '',
      component: HomeComponent,
      canActivate: [AuthGuard],
      children: [{
        path: '',
        component: MainComponent
      }, {
        path: 'import',
        component: ImportComponent
      }, {
        path: 'export',
        component: ExportComponent
      }, {
        path: 'view',
        component: ViewComponent
      }, {
        path: 'creator',
        component: CreatorComponent
      }, {
        path: 'invoice',
        component: InvoiceComponent
      }, {
        path: 'profile',
        component: ProfileComponent
      }
      ]
    },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' },

    // testing
    // {
    //   path: 'gallery',
    //   component: GalleryComponent,
    //   data: { title: 'List of Sales' }
    // },
    // {
    //   path: 'gallery-details/:id',
    //   component: GalleryDetailsComponent,
    //   data: { title: 'Sales Details' }
    // },
    // { path: '',
    //   redirectTo: '/gallery',
    //   pathMatch: 'full'
    // }
];

export const routing = RouterModule.forRoot(appRoutes);
