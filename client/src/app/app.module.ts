﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptorProvider, ErrorInterceptorProvider } from './_helpers';
import {AlertService, AuthenticationService, UploadService, UserService} from './_services';

import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import {AlertModule} from './_alert';

import {MatSidenavModule} from '@angular/material/sidenav';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { HomeModule } from './home/home.module';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AlertModule,
    HomeModule,
    MatSidenavModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    routing

  ],
    declarations: [
        AppComponent,
        AlertComponent,
        LoginComponent,
        RegisterComponent
  ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        UploadService,
        JwtInterceptorProvider,
        ErrorInterceptorProvider,
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
