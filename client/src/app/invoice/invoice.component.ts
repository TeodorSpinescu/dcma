import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {ExtractService} from '../_services/extract.service';
import {Client} from '../_models/client';
import {faAddressBook, faMailBulk, faPersonBooth, faPhone, faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  providers: [ExtractService]
})


export class InvoiceComponent implements OnInit {
  check: boolean;

  emailIcon = faMailBulk;
  adressIcon = faAddressBook;
  personIcon = faPhone;
  trashIcon = faTrashAlt;

  constructor(
    public extractService: ExtractService,
    private router: Router,
    private toastr: ToastrService,
  ) {
  }

  ngOnInit(): void {
    this.refreshClientsList();
    console.log(this.check);
  }

  refreshClientsList(): void {
    this.extractService.getClientList().subscribe(res => {
      this.extractService.clients = res as Client[];
    });
  }

  deleteClient(id: string): void {
    this.extractService.deleteClient(id).subscribe(res => {
      location.reload();
    });
    this.toastr.warning('Your Client has been deleted', 'Delete', {
      timeOut: 6000,
      positionClass: 'toast-bottom-right'
    });
  }

  createNewInvoice(id: string): void {
    this.extractService.getClientById(id).subscribe(res => {
      this.extractService.clientInvoice = res as Client;
      localStorage.setItem('currentClient', JSON.stringify(this.extractService.clientInvoice));
    });

    this.check = true;
    localStorage.setItem('currentClientCheck', JSON.stringify(this.check));
    this.router.navigate(['/creator'])
      .then((res) => {
        location.reload();
        this.toastr.info('Client Added to Invoice Creator', 'Added', {
          timeOut: 6000,
          positionClass: 'toast-bottom-right'
        });
      });
  }
}
