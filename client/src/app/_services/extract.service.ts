import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Client} from '../_models/client';
import {PdfExtra} from '../_models/pdfExtra';
import {Gallery} from '../_models';

@Injectable({
  providedIn: 'root'
})
export class ExtractService {

  clientInvoice: Client;
  clients: Client[];
  readonly extractURL = 'http://localhost:4000/view/extract';
  readonly invoiceURL = 'http://localhost:4000/invoice';
  readonly creatorURL = 'http://localhost:4000/creator';
  readonly importURL = 'http://localhost:4000/creator/generate';

  constructor(private http: HttpClient) {}

  getClientList(): Observable<any> {
    return this.http.get(this.invoiceURL);
  }

  getClientById(id: string): Observable<any> {
    const clientURL = `${this.invoiceURL}/${id}`;
    return this.http.get(clientURL);
  }

  deleteClient(id: string): Observable<any> {
    const URL = `${this.invoiceURL}/${id}`;
    return this.http.delete(URL);
  }

  extractData(id: string): Observable<any> {
      const URL = `${this.extractURL}/${id}`;
      return this.http.post(URL, id);
  }

  generatePDF(pdfExtra: PdfExtra): Observable<any> {
    const formData = new FormData();
    formData.append('item', pdfExtra.item);
    formData.append('quantity', pdfExtra.quantity);
    formData.append('value', pdfExtra.value);
    formData.append('unit', pdfExtra.unit);
    formData.append('company', pdfExtra.company);
    formData.append('owner', pdfExtra.owner);
    formData.append('adress', pdfExtra.adress);
    formData.append('cif', pdfExtra.cif);
    formData.append('pick', pdfExtra.pick);

    return this.http.post(this.creatorURL, formData);
  }

  addPDF(gallery: Gallery, file: File): Observable<any> {

    const formData = new FormData();
    formData.append('file', file);
    formData.append('fileUser', gallery.fileUser);
    formData.append('imageDesc', gallery.imageDesc);

    console.log(gallery.fileUser);

    const header = new HttpHeaders();
    const params = new HttpParams();

    const options = {
      params,
      headers: header,
      // responseType: 'json' as 'json'
    };
    const req = new HttpRequest('POST', this.importURL, formData, options);
    return this.http.request(req);
  }
}
