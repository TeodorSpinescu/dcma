import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams, HttpRequest } from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Gallery, User} from '../_models';
import {appConfig} from '../app.config';

@Injectable()
export class UploadService {

  documents: Gallery[];
  readonly viewURL = 'http://localhost:4000/view';
  readonly importURL = 'http://localhost:4000/import';

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  // tslint:disable-next-line:typedef
  getDocumentsList() {
    return this.http.get(this.viewURL);
  }

  getGalleryById(id: string): Observable<any> {
    const url = `${appConfig.apiUrl}/${id}`;
    return this.http.get<Gallery>(url).pipe(
      catchError(this.handleError)
    );
  }

  addGallery(gallery: Gallery, file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('fileUser', gallery.fileUser);
    formData.append('imageDesc', gallery.imageDesc);
    const header = new HttpHeaders();
    const params = new HttpParams();

    const options = {
      params,
      headers: header,
      // responseType: 'json' as 'json'
    };
    const req = new HttpRequest('POST', this.importURL, formData, options);
    return this.http.request(req);
  }
}
