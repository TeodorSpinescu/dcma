import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DownloadService {

  readonly downloadURL = 'http://localhost:4000/view/download';
  readonly deleteURL = 'http://localhost:4000/view';

  constructor(private http: HttpClient) { }

  downloadFile(id: string): Observable<any> {
    const URL = `${this.downloadURL}/${id}`;
    return this.http.get(URL, {
      observe: 'response',
      responseType: 'blob'
    });
  }

  deleteFile(id: string): Observable<any> {
    const URL = `${this.deleteURL}/${id}`;
    return this.http.delete(URL);
  }
}
