import { Component, OnInit } from '@angular/core';
import {faCloudUploadAlt} from '@fortawesome/free-solid-svg-icons';
import {Gallery, User} from '../_models';
import {ActivatedRoute, Router} from '@angular/router';
import {UploadService, UserService} from '../_services';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';

import {ErrorStateMatcher} from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {
  uploadCloud = faCloudUploadAlt;

  galleryForm: FormGroup;
  imageFile: File = null;
  fileUser = '';
  imageDesc = '';
  isLoadingResults = false;
  matcher = new MyErrorStateMatcher();

  gallery = Gallery;

  currentUser: User;

  constructor(
    private api: UploadService,
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit(): void {
    this.galleryForm = this.formBuilder.group({
      imageFile : [null, Validators.required],
      fileUser : [null, Validators.required],
      imageDesc : [null, Validators.required]
    });
  }

  onFormSubmit(): void {
    this.isLoadingResults = true;
    this.galleryForm.value.fileUser = this.currentUser.username;
    this.api.addGallery(this.galleryForm.value, this.galleryForm.get('imageFile').value._files[0])
      .subscribe((res) => {
        this.isLoadingResults = false;
        if (res) {
          this.router.navigate(['/view']).then(r => location.reload());
        }
      }, (err: any) => {
        console.log('Eroare in incarcarea fisierului: ', err);
        this.isLoadingResults = false;
      });
  }
}
