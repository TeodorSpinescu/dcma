import { Component, OnInit } from '@angular/core';
import {ExtractService} from '../_services/extract.service';
import {Client} from '../_models/client';
import {faAddressBook, faMailBulk, faPhone} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';

import { PDFDocument, StandardFonts, rgb } from 'pdf-lib';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from "../_models";

@Component({
  selector: 'app-creator',
  templateUrl: './creator.component.html',
  styleUrls: ['./creator.component.css'],
  providers: [ExtractService]
})
export class CreatorComponent implements OnInit {

  currentClient: Client;
  checkingClient: boolean;

  emailIcon = faMailBulk;
  adressIcon = faAddressBook;
  personIcon = faPhone;

  galleryForm: FormGroup;
  newInvoiceForm: FormGroup;
  radioForm: FormGroup;

  imageFile: File = null;
  currentUser: User;


  constructor(
    private extractService: ExtractService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.currentClient = JSON.parse(localStorage.getItem('currentClient'));
    this.checkingClient = JSON.parse(localStorage.getItem('currentClientCheck'));
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  }

  ngOnInit(): void {
    this.newInvoiceForm = this.formBuilder.group({
      item : [null, Validators.required],
      quantity : [null, Validators.required],
      value : [null, Validators.required],
      unit : [null, Validators.required],
      company : [null, Validators.required],
      owner : [null, Validators.required],
      adress : [null, Validators.required],
      cif : [null, Validators.required],
      pick: [null]
    });

    this.galleryForm = this.formBuilder.group({
      imageFile : [null, Validators.required],
      fileUser : [null, Validators.required],
      imageDesc : [null, Validators.required]
    });

    this.radioForm = this.formBuilder.group({
      template: [null, Validators.required],
    });
  }

  selectOther(): void {
    localStorage.setItem('currentClientCheck', JSON.stringify(false));
    this.router.navigate(['/invoice']);
  }

  refreshPage(): void {
    location.reload();
  }

  checkData(): boolean {
    const formValues = this.newInvoiceForm.value.item &&
      this.newInvoiceForm.value.quantity &&
      this.newInvoiceForm.value.unit &&
      this.newInvoiceForm.value.value;
    return this.radioForm.value.template != null && this.checkingClient && formValues;
  }

  createPdf(): void {
    this.newInvoiceForm.value.company = this.currentClient.companyName;
    this.newInvoiceForm.value.owner = this.currentClient.ownerName;
    this.newInvoiceForm.value.adress = this.currentClient.adress;
    this.newInvoiceForm.value.cif = this.currentClient.cifNumber;
    this.newInvoiceForm.value.pick = this.radioForm.value.template;

    console.log(this.newInvoiceForm.value.pick);

    this.extractService.generatePDF(this.newInvoiceForm.value).subscribe((res) => {
      this.galleryForm.value.fileUser = this.currentUser.username;
      this.extractService.addPDF(this.galleryForm.value, res).subscribe();
      this.router.navigate(['/view']).then(r => location.reload());
    });

  }

}
