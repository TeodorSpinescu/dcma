// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
//
// import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
// import {SidenavComponent} from './sidenav.component';
// import {ImportComponent} from '../import/import.component';
//
// import {MatSidenavModule} from '@angular/material/sidenav';
// import {MaterialModuleModule} from '../material-module/material-module.module';
// import {MatListModule} from '@angular/material/list';
// import {MatIconModule} from '@angular/material/icon';
// import {MatButtonModule} from '@angular/material/button';
// import {MatBadgeModule} from '@angular/material/badge';
//
// import {MatMenuModule} from '@angular/material/menu';
// import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
// import {FormsModule} from '@angular/forms';
// import {RouterModule} from '@angular/router';
//
//
//
// @NgModule({
//   declarations: [
//     SidenavComponent,
//     ImportComponent
//   ],
//   imports: [
//     CommonModule,
//     MatSidenavModule,
//     BrowserAnimationsModule,
//     NoopAnimationsModule,
//     MaterialModuleModule,
//     MatListModule,
//     MatIconModule,
//     MatButtonModule,
//     MatBadgeModule,
//     MatMenuModule,
//     FormsModule,
//     FontAwesomeModule,
//     RouterModule,
//   ],
//   exports: [
//     SidenavComponent
//   ]
// })
// export class SidenavModule { }
