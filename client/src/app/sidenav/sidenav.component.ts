import { Component, OnInit } from '@angular/core';
import {
  faExchangeAlt,
  faFileExport,
  faFileInvoice,
  faFileUpload, faFlag, faHome,
  faList, faPaperPlane, faSearch, faStream, faUserAlt,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  opened = false;
  mainPage = faHome;
  fileUpload = faFileUpload;
  fileExport = faFileExport;
  viewFiles = faList;
  pdfCreator = faExchangeAlt;
  invoiceGenerator = faFileInvoice;
  userProfile = faUserAlt;
  sidenavToggle = faStream;
  searchTool = faSearch;
  mailNotification = faPaperPlane;
  flag = faFlag;


  constructor() { }

  ngOnInit(): void {
  }

}
